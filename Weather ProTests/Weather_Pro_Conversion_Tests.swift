//
//  Weather_Pro_Conversion_Tests.swift
//  Weather Pro
//
//  Created by Tashi Ny. on 14/04/2017.
//  Copyright © 2017 Tashi Ny. All rights reserved.
//

import XCTest
import MapKit
@testable import Weather_Pro

class Weather_Pro_Conversion_Tests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testFahrenheitConversions() {
        
        let fahToCelArr = [(-459.67,-273.15),
                       (-50,-45.56),
                       (-40 ,-40.00),
                       (-30 ,-34.44),
                       (-20 ,-28.89),
                       (-10 ,-23.33),
                       (0 ,-17.78)]
        
        for (fahrenheit, celsius) in fahToCelArr {
            
            let converted = Temperature.convert(from: .Fahrenheit, temperature: fahrenheit, to: .Celsius).roundToDecimal(2)
            XCTAssertEqual(converted, celsius, "Conversion Unseccessful \(converted) =\\= \(celsius)")
        }
        
        let fahToKelArr = [(-459.67 ,0),
            (-50 ,227.59),
            (-40 ,233.15),
            (-30 ,238.71),
            (-20 ,244.26),
            (-10 ,249.82),
            (0 ,255.37),
            (10 ,260.93),
            (20 ,266.48),
            (30 ,272.04),
            (40 ,277.59),
            (50 ,283.15),
            (60 ,288.71),
            (70 ,294.26),
            (80 ,299.82),
            (90 ,305.37),
            (100 ,310.93),]
        
        for (fahrenheit, kelvin) in fahToKelArr {
            
            let converted = Temperature.convert(from: .Fahrenheit, temperature: fahrenheit, to: .Kelvin).roundToDecimal(2)
            XCTAssertEqual(converted, kelvin, "Conversion Unseccessful \(converted) =\\= \(kelvin)")
        }
        
    }
    
    func testKelvinConversions() {
        
        let kelToCelArr = [(0, -273.15),
            (10, -263.15),
            (20, -253.15),
            (30, -243.15),
            (40, -233.15),
            (50, -223.15),
            (60, -213.15),
            (70, -203.15),
            (80, -193.15),
            (90, -183.15),
            (100, -173.15),
            (110, -163.15),
            (120, -153.15),
            (130, -143.15),
            (140, -133.15),
            (150, -123.15),
            (160, -113.15),
            (170, -103.15),
            (180, -93.15),
            (190, -83.15),
            (200, -73.15),
            (210, -63.15),
            (220, -53.15),
            (230, -43.15),
            (240, -33.15),
            (250, -23.15),
            (260, -13.15),
            (270, -3.15),
            (273.15, 0)]
        
        for (kelvin, celsius) in kelToCelArr {
            
            let converted = Temperature.convert(from: .Kelvin, temperature: kelvin, to: .Celsius).roundToDecimal(2)
            XCTAssertEqual(converted, celsius, "Conversion Unseccessful \(converted) =\\= \(celsius)")
        }
        
        let kelToFahArr = [(0.0, -459.67),
            (10, -441.67),
            (20, -423.67),
            (30, -405.67),
            (40, -387.67),
            (50, -369.67),
            (60, -351.67),
            (70, -333.67),
            (80, -315.67),
            (90, -297.67),
            (100, -279.67),
            (110, -261.67),
            (120, -243.67),
            (130, -225.67),
            (140, -207.67),
            (150, -189.67),
            (160, -171.67),
            (170, -153.67),
            (180, -135.67),
            (190, -117.67),
            (200, -99.67),
            (210, -81.67),
            (220, -63.67)]
        
        for (kelvin, fahrenheit) in kelToFahArr {
            
            let converted = Temperature.convert(from: .Kelvin, temperature: kelvin, to: .Fahrenheit).roundToDecimal(2)
            XCTAssertEqual(converted, fahrenheit, "Conversion Unseccessful \(converted) =\\= \(fahrenheit)")
        }
    }
    
    func testCelsiusConversions() {
        
        let celToKelArr = [(-273.15, 0),
            (-50, 223.15),
            (-40, 233.15),
            (-30, 243.15),
            (-20, 253.15),
            (-10, 263.15),
            (0, 273.15),
            (10, 283.15),
            (20, 293.15)]
        
        for (celsius, kelvin) in celToKelArr {
            
            let converted = Temperature.convert(from: .Celsius, temperature: celsius, to: .Kelvin).roundToDecimal(2)
            XCTAssertEqual(converted, kelvin, "Conversion Unseccessful \(converted) =\\= \(kelvin)")
        }
        
        let celToFahArr = [(-50.0, -58.0),
            (-40, -40.0),
            (-30, -22.0),
            (-20, -4.0),
            (-10, 14.0),
            (-9, 15.8),
            (-8, 17.6),
            (-7, 19.4),
            (-6, 21.2),
            (-5, 23.0),
            (-4, 24.8),
            (-3, 26.6),
            (-2, 28.4),
            (-1, 30.2),
            (0, 32.0),
            (1, 33.8),
            (2, 35.6),
            (3, 37.4),
            (4, 39.2),
            (5, 41.0),
            (6, 42.8),
            (7, 44.6),
            (8, 46.4),
            (9, 48.2),
            (10, 50.0)]
        
        for (celsius, fahrenheit) in celToFahArr {
            
            let converted = Temperature.convert(from: .Celsius, temperature: celsius, to: .Fahrenheit).roundToDecimal(1)
            XCTAssertEqual(converted, fahrenheit, "Conversion Unseccessful \(converted) =\\= \(fahrenheit)")
        }
    }
}
