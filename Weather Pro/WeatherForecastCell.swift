//
//  WeatherForecastCell.swift
//  Weather Pro
//
//  Created by Tashi Ny. on 14/04/2017.
//  Copyright © 2017 Tashi Ny. All rights reserved.
//

import Foundation
import UIKit

class WeatherForecastCell: UITableViewCell {
    
    @IBOutlet weak var dayOfTheWeekLabel: UILabel!
    @IBOutlet weak var weatherConditionLabel: UILabel!
    @IBOutlet weak var highTempLabel: UILabel!
    @IBOutlet weak var lowTempLabel: UILabel!
}
