//
//  WeatherAPI.swift
//  Weather Pro
//
//  Created by Tashi Ny. on 14/04/2017.
//  Copyright © 2017 Tashi Ny. All rights reserved.
//

import Foundation
import MapKit

class WeatherAPI {
    
    struct API {
        
        var base: String {
            return "http://api.openweathermap.org/"
        }
        
        var version: String {
            return "2.5"
        }
        
        var appID: String {
            return "163a67014f43522cf803273eb6b9377a"
        }
    }
    
    class func fetchWeatherReport(at location:CLLocationCoordinate2D, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Swift.Void) {
        
        let urlString = "\(API().base)/data/\(API().version)/forecast?lat=\(String(describing: location.latitude))&lon=\(String(describing: location.longitude))&APPID=\(API().appID)"
        
        guard let url = URL(string: urlString) else {
            print("Error: cannot create URL")
            return
        }
        
        let urlRequest = URLRequest(url: url)
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = Foundation.URLSession(configuration: config)
        
        let task = session.dataTask(with: urlRequest, completionHandler: completionHandler)
        
        task.resume()

    }
    
}
