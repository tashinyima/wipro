//
//  Conversions.swift
//  Weather Pro
//
//  Created by Tashi Ny. on 14/04/2017.
//  Copyright © 2017 Tashi Ny. All rights reserved.
//

import Foundation

enum TemperatureType: Int {
    case Kelvin
    case Celsius
    case Fahrenheit
}

class Temperature {
    
    
    /// Converts between the 3 temperature types; Kelvin, Celsius & Fahrenheit
    /// Uses http://www.rapidtables.com/convert/temperature/
    /// - Parameters:
    ///   - oldType: The old temperature type
    ///   - temperature: The temperature
    ///   - newType: The new temperature type
    /// - Returns: The converted temperature
    class func convert(from oldType: TemperatureType, temperature: Double, to newType:TemperatureType) -> Double {
        
        var newTemperature = 0.0
        
        switch (oldType, newType) {
            
        // The temperature T in degrees Celsius (°C) is equal to the temperature T in Kelvin (K) minus 273.15:
        // T(°C) = T(K) - 273.15
        case (.Kelvin, .Celsius):
            newTemperature = temperature - 273.15
            
        // The temperature T in degrees Fahrenheit (°F) is equal to the temperature T in Kelvin (K) times 9/5, minus 459.67:
        // T(°F) = T(K) × 9/5 - 459.67
        case (.Kelvin, .Fahrenheit):
            newTemperature = (temperature * 9.0/5.0) - 459.67
            
        // The temperature T in degrees Fahrenheit (°F) is equal to the temperature T in degrees Celsius (°C) times 9/5 plus 32:
        // T(°F) = T(°C) × 9/5 + 32
        case (.Celsius, .Fahrenheit):
            newTemperature = (temperature * 9.0/5.0) + 32.0
            
        // The temperature T in Kelvin (K) is equal to the temperature T in degrees Celsius (°C) plus 273.15:
        // T(K) = T(°C) + 273.15
        case (.Celsius, .Kelvin):
            newTemperature = temperature + 273.15
        
        // The temperature T in degrees Celsius (°C) is equal to the temperature T in degrees Fahrenheit (°F) minus 32, times 5/9:
        // T(°C) = (T(°F) - 32) × 5/9
        case (.Fahrenheit, .Celsius):
            newTemperature = (temperature - 32.0) * (5.0/9.0)
            
        // The temperature T in Kelvin (K) is equal to the temperature T in degrees Fahrenheit (°F) plus 459.67, times 5/9:
        // T(K) = (T(°F) + 459.67) × 5/9
        case (.Fahrenheit, .Kelvin):
            newTemperature = (temperature + 459.67) * (5.0/9.0)
            
        // All other cases are handled by this === case(_, _)
        default:
            newTemperature = temperature
        }
        
        return newTemperature
    }
}

extension Double {
    func roundToDecimal(_ fractionDigits: Int) -> Double {
        let multiplier = pow(10, Double(fractionDigits))
        return Darwin.round(self * multiplier) / multiplier
    }
}
