//
//  WeatherForecastViewController.swift
//  Weather Pro
//
//  Created by Tashi Ny. on 14/04/2017.
//  Copyright © 2017 Tashi Ny. All rights reserved.
//

import Foundation
import UIKit

class WeatherForecastViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let forecast = Forecast()
    var dateFormatter = DateFormatter()
    
    @IBOutlet weak var cityTitle: UILabel!
    @IBOutlet weak var currentConditionsLabel: UILabel!
    @IBOutlet weak var currentTemperatureLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        dateFormatter.dateFormat = "EEEE"
        dateFormatter.locale = Locale(identifier: "en_GB")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getForecast()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func getForecast() {
        
        if let currentLocation = self.forecast.currentPosition {
            
            WeatherAPI.fetchWeatherReport(at: currentLocation, completionHandler: { (data, response, error) in
                // check for errors
                guard error == nil else {
                    print("error calling GET on \(response?.url?.absoluteURL)")
                    print(error as Any)
                    return
                }
                
                // check for data
                guard let responseData = data else {
                    print("Error: did not receive data")
                    return
                }
                
                // parse the responseData as JSON
                do {
                    guard let parsedData = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: Any] else {
                        print("error trying to convert data to JSON")
                        return
                    }
                    
                    let cityData = parsedData["city"] as! [String:Any]
                    
                    guard let cityName = cityData["name"] as? String else {
                        print("Could not get cityName from JSON")
                        return
                    }
                    
                    let forecastData = parsedData["list"] as! [[String: Any]]
                    
                    var array = [Any]()
                    
                    for index in 0..<forecastData.count {
                        
                        let element = forecastData[index]
                        if index == 0 {
                            let main = element["main"] as! [String:Any]
                            if let currentTemperature = main["temp"] as? Double {
                                let convertTemp = Temperature.convert(from: .Kelvin, temperature: currentTemperature, to: .Celsius)
                                self.forecast.currentTemperature = "\(convertTemp.roundToDecimal(1))"
                            }
                            
                            if let weather = element["weather"] as? [[String: Any]] {
                                let contents = weather[0]
                                if let currentConditions = contents["description"] as? String {
                                    self.forecast.currentConditions = currentConditions
                                }
                            }
                            array.append(element)
                            
                        } else {
                            
                            if let dateString = element["dt_txt"] as? String {
                                if (dateString.hasSuffix("12:00:00")) {
                                    if array.count < 5 {
                                        array.append(element)
                                    }
                                }
                            }
                        }
                    }
                    
                    self.forecast.forecastIntervals = array
                    
                    DispatchQueue.main.async {
                        self.cityTitle.text = cityName
                        self.currentConditionsLabel.text = self.forecast.currentConditions
                        self.currentTemperatureLabel.text = "\(self.forecast.currentTemperature)\u{00B0}"
                        self.tableView.reloadData()
                    }
                } catch {
                    print("failed to serialize data to JSON")
                    return
                }

            })
        }
    }
    
    // MARK: tableview methods go here
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        return forecast.forecastIntervals.count
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: "weatherCell",
            for: indexPath) as! WeatherForecastCell
        
        let element = self.forecast.forecastIntervals[indexPath.row] as! [String: Any]
        let date = NSDate(timeIntervalSince1970: element["dt"] as! TimeInterval)
        cell.dayOfTheWeekLabel.text = dateFormatter.string(from:date as Date)
        
        if let weather = element["weather"] as? [[String: Any]] {
            let contents = weather[0]
            if let currentConditions = contents["description"] as? String {
                cell.weatherConditionLabel.text = currentConditions
            }
        }
        
        let main = element["main"] as! [String:Any]
        if let tempMax = main["temp_max"] as? Double {
            let convertTemp = Temperature.convert(from: .Kelvin, temperature: tempMax, to: .Celsius)
            cell.highTempLabel.text = "\(convertTemp.roundToDecimal(1))"

        }
        if let tempMin = main["temp_min"] as? Double {
            let convertTemp = Temperature.convert(from: .Kelvin, temperature: tempMin, to: .Celsius)
            cell.lowTempLabel.text = "\(convertTemp.roundToDecimal(1))"
          
        }
        return cell
    }
}
