//
//  Forecast.swift
//  Weather Pro
//
//  Created by Tashi Ny. on 14/04/2017.
//  Copyright © 2017 Tashi Ny. All rights reserved.
//

import Foundation
import CoreLocation

class Forecast {
    
    var currentPosition: CLLocationCoordinate2D? = nil
    var forecastIntervals = [Any]()
    var currentTemperature = ""
    var currentConditions = ""
    
}
